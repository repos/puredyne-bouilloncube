#!/usr/bin/env python
# Simple tool to gather information on a machine running pure:dyne

import subprocess

# Who are you?
res = subprocess.Popen(['cat', '/etc/hostname'], stdout=subprocess.PIPE)
hostname = res.stdout.read().strip()

# Kernel
uname = "uname -a"
print "Gathering system information with %s command:\n" % uname
subprocess.call(uname, shell=True)

# Memory
diskspace = "df -h"
print "Gathering diskspace information with %s command:\n" % diskspace
subprocess.call(diskspace, shell=True)


