#!/usr/bin/env python
#!/usr/bin/env python


# make-usb-key.py can be used to produce new USB keys from
# an existing ISO or directly from a live session
#
#    (C) Copyright 2009-2010 Puredyne Team
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import sys, os, subprocess
import time, shutil


##if sys.platform.find('windows') > -1 :
##    print 'sorry, it does not work on windows'
##    sys.exit()


import pygtk
pygtk.require('2.0')
import gtk
import threading
import gobject



# GTK to do :

# add to progress bar the copy of 680 megas of live folder. need another thread
# control size of window. it grows...
# pop up when USB is ready... but this must be done from outside the thread, otheriwse error


# deploying the mess
ISOMOUNT = "/mnt/make-live-iso"
DEVMOUNT = "/mnt/make-live-device"



target = ''
source = 'live'

thread = 0 
log = 1 # will write prints to log file

# grub config files path **** FIX THIS ****
grubfiles = '/home/r2d2/bouilloncube/py' # os.getcwd()

logopath = '/usr/share/xfce4/backdrops/puredyne-logo-desktop.png'

helptxt = """PureDyne live USB creator allows you to turn any device into
a Puredyne live medium, such as a liveUSB or liveHD.
Two arguments are required: sourceimage, and targetdevice.

Examples:
- To create a liveUSB from an ISO
Select 'iso' option, choose a Pure Dyne .iso file and a target device /dev/yourdevice

- To create a liveHD from a running liveCD
Select 'live' option and a target device /dev/yourdevice

It saves a log file to /var/log with output from the process
"""

abouttxt = {
            'name': 'PureDyne live USB creator',
            'version' : '0.1',
            'copyright' :'(c) puredyne',
            'comments' : 'creates live USBs',
            'website' :'http://www.puredyne.org'
            }




def runCmd(cmd='') :
    res = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE).communicate()[0]
    if res != '': 
        print res # write to log 
        progressbar.set_text(res)



def feedback(string) :
    print string # writes to log with log == True
    try :
        progressbar.set_text(string) 
        time.sleep(0.1)
    except : # progressbar not ready yet? skip
        pass




def customcopytree(src, dst):
    """ custom version of shutil.copytree
        only diferences are :
            - removes dst if already present
            - verbose copy each file
            - removed ignore and symlinks args
            - removed copystat changging copy2 o copy
            - added print errors for log file
        i would like to speed it up as much as possible
        more feedback on copy progress?
    """
    if os.path.isdir(dst) :
        try :
            shutil.rmtree(dst)
            feedback('[*] removing old %s directory tree from destination' % dst)
        except OSError , why:
            print 'customcopytree()', OSError, why

        
    names = os.listdir(src)

    os.makedirs(dst)
    errors = []
    
    for name in names :
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try :
            if os.path.isdir(srcname):
                customcopytree(srcname, dstname) 
            else :
                current = srcname.split('/')[-1]
                feedback('[*] ... copying %s to %s' % (current, target))
                shutil.copy(srcname, dstname)
                
        except (IOError, os.error), why :
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except shutil.Error, err:
            errors.extend(err.args[0])
            
    if errors :
        print shutil.Error, errors # write to log
        raise shutil.Error, errors





def make_live_device(source='live', target=''):
    global ISOMOUNT, DEVMOUNT
   
    # unpack the present
    if source == 'live' :
        ISOMOUNT = "/live/image"
        # Calculate size of first partition
        ISOSIZEB = float(os.popen("du -sb /live/image | cut -f1", "r").read()) #`du -sb /live/image | cut -f1`
        ISOSIZEMB = ISOSIZEB / 1000000 #`expr $ISOSIZEB / 1000000`
        feedback("[*] ISO file is %i MB" % ISOSIZEMB)
    else :
        # Mount ISO file to $ISOMOUNT
        feedback(  "[*] mounting ISO" )
        time.sleep(0.1)
        if not os.path.isdir(ISOMOUNT) :
            os.mkdir(ISOMOUNT)
            
        #mount -o loop $1 $ISOMOUNT
        runCmd("mount -o loop %s %s" % (source, ISOMOUNT))

        # Calculate size of first partition
        ISOSIZEB = os.stat(source)[6] # size ?? need to remove the last character?
        ISOSIZEMB = ISOSIZEB / 1000000 #ISOSIZEMB=`expr $ISOSIZEB / 1000000`
        feedback("[*] ISO file is %i MB" % (ISOSIZEB))


    progressbar.set_fraction(progressbar.get_fraction() + 0.05) # 5 %
        
    # Clean target
    feedback("[*] force unmounting partitions from %s" % target)
    
    ##sudo umount ${2}*
    runCmd('umount %s*' % target)
    
    feedback("[*] erasing target's partition table")
    ##parted -s $2 mklabel msdos
    runCmd('parted -s %s mklabel msdos' % target)
    ##partprobe
    runCmd('partprobe')
    ##sleep 5
    time.sleep(5)
    ##umount ${2}*
    runCmd("umount %s" % target)
    
    # prepare system partition
    SYSPART = ISOSIZEMB + 100 #`expr $ISOSIZEMB + 100`
    feedback("[*] creating system partition of %i MB" % (SYSPART))
    
    ##parted -s $2 mkpart primary ext2 1MB ${SYSPART}MB
    runCmd("parted -s %s mkpart primary ext2 1MB %sMB" % (target, SYSPART))

    progressbar.set_fraction(progressbar.get_fraction() + 0.02) # 7%

    ##parted -s $2 set 1 boot on
    runCmd("parted -s %s set 1 boot on" % target)

    progressbar.set_fraction(progressbar.get_fraction() + 0.02) # 9%

    feedback("[*] force unmounting partition %s1" % (target))
    time.sleep(5)
    ##umount ${2}1
    runCmd("umount %s1" % target)
    ## mkfs.ext2 -L puredyne ${2}1
    runCmd("mkfs.ext2 -L puredyne %s1" % (target))

    progressbar.set_fraction(progressbar.get_fraction() + 0.06) # 15%

    # prepare persistence partition
    feedback("[*] creating persistence partition")
    ##parted -s $2 mkpart primary ext2 ${SYSPART}MB -- -1M
    runCmd("parted -s %s mkpart primary ext2 %sMB -- -1M" % (target, SYSPART))
    progressbar.set_fraction(progressbar.get_fraction() + 0.05) # 20 %
    
    feedback("[*] force unmounting partition %s2" % (target))
    time.sleep(5)
    ##umount ${2}2
    runCmd("umount %s2" % target)
    ##mkfs.ext2 -L live-rw ${2}2
    runCmd("mkfs.ext2 -L live-rw %s2" % target)
    progressbar.set_fraction(progressbar.get_fraction() + 0.05) # 25%

    # copy live folder
    feedback( "[*] copying live folder" )
    ##if [ ! -d $DEVMOUNT ]; then
    ##    mkdir $DEVMOUNT
    ##fi
    if not os.path.isdir(DEVMOUNT) :
        feedback( "[*] debug --> creating %s" % (DEVMOUNT) )
        os.mkdir(DEVMOUNT)
            

    ##mount ${2}1 $DEVMOUNT
    runCmd("mount %s1 %s" % (target, DEVMOUNT))

    feedback("[*] grub configuration")

    ## this was a cramp!
    ##BLKID=`blkid | grep ${2}1 | awk '{ for(i=1;i<NF;i++) { if( $i ~ "UUID" ) { print $i } } }' | cut -d\" -f 2`
    BLKID = ''
    stuff = subprocess.Popen(['blkid'], stdout=subprocess.PIPE).communicate()[0]
    stuff = stuff.split(' ') # convert to a list
    for i, v in enumerate(stuff) :
        if v.find( target+'1' ) > -1: # get the UUID of the 1st partition in 2nd argv 
            BLKID = stuff[i+2][6:-1] # extract id from UUID="id"

    feedback( "[*] device ID is %s" % BLKID )

    progressbar.set_fraction(progressbar.get_fraction() + 0.02) # 27%

    ##grub-install --no-floppy --root-directory=${DEVMOUNT} $2
    runCmd("grub-install --no-floppy --root-directory=%s %s" % (DEVMOUNT, target))
    progressbar.set_fraction(progressbar.get_fraction() + 0.05) # 32%

    ### temp fix (see launchpad bugs...)
    ##cat $(dirname "${0}")"/grub-puredyne-911.cfg.in" | sed -e 's/\${BLKID}/'${BLKID}'/g' > "${DEVMOUNT}/boot/grub/grub.cfg"
    ##cp "/usr/share/grub/unicode.pf2" "${DEVMOUNT}/boot/grub/"
    ##cp $(dirname "${0}")"/grub-puredyne-911.png" "${DEVMOUNT}/boot/grub/"
    
    txt = open('%s/grub-puredyne-911.cfg.in' % grubfiles, 'rU').read()
    txt = txt.replace('${BLKID}', BLKID)
    grub = open('%s/boot/grub/grub.cfg' % DEVMOUNT, 'w')
    grub.write(txt)
    grub.close()
    shutil.copy('/usr/share/grub/unicode.pf2' , DEVMOUNT+'/boot/grub/')
    shutil.copy('%s/grub-puredyne-911.png' % grubfiles , DEVMOUNT+'/boot/grub/')

    progressbar.set_fraction(progressbar.get_fraction() + 0.05) # 37%
        
    # copy all the data
    ##pushd $ISOMOUNT
    ## cp -rv live/ LICENSE extra md5sum.txt pure.seed README  $DEVMOUNT
    
    # 679 Megas!!! this file only it takes few minutes!!
    feedback( "[*] ... copying %s/live folder to %s" % (ISOMOUNT, DEVMOUNT) )
    customcopytree('%s/live'%ISOMOUNT, '%s/live'%DEVMOUNT)
    
    progressbar.set_fraction(progressbar.get_fraction() + 0.33) # 70%

    feedback( "[*] ... copying %s/LICENSE file to %s" % (ISOMOUNT, DEVMOUNT) )
    shutil.copy('%s/LICENSE'%ISOMOUNT, DEVMOUNT)
        
    feedback( "[*] ... copying %s/extra folder to %s" % (ISOMOUNT, DEVMOUNT) )
    customcopytree('%s/extra'%ISOMOUNT, '%s/extra'%DEVMOUNT)

    progressbar.set_fraction(progressbar.get_fraction() + 0.10) # 80%

    feedback( "[*] ... copying %s/md5sum.txt file to %s" % (ISOMOUNT, DEVMOUNT) )
    shutil.copy('%s/md5sum.txt'%ISOMOUNT, DEVMOUNT)

    feedback("[*] ... copying %s/pure.seed file to %s" % (ISOMOUNT, DEVMOUNT))
    shutil.copy('%s/pure.seed'%ISOMOUNT, DEVMOUNT)

    feedback( '[*] ... copying %s/README file to %s' % (ISOMOUNT, DEVMOUNT) )
    shutil.copy('%s/README'%ISOMOUNT, DEVMOUNT)

    progressbar.set_fraction(progressbar.get_fraction() + 0.1) # 90%

    ##pushd

    # done
    feedback("[*] cleaning up, unmounting and ...")
    
    if source != 'live' : # remove file
        runCmd("umount %s" % ISOMOUNT) # umount $ISOMOUNT
        
    runCmd("umount %s" % DEVMOUNT) # umount $DEVMOUNT
    runCmd('umount %s*' % target) # mount ${2}*

    if source != 'live' : #  RECURSIVE FORCE remove file
        shutil.rmtree(ISOMOUNT) # rm -rf $ISOMOUNT

    #rm -rf $DEVMOUNT
    shutil.rmtree(DEVMOUNT) # force recursive delete file structure

    progressbar.set_fraction(progressbar.get_fraction() + 0.1) # 100%??

    feedback("[*] ...done.")





### GUI GTK functions ###

##def callback(widget, data):
##    feedback('%s was pressed' % data)

    

def cboxSelected(widget, data):
    global target
    if cbox.get_active() > 0 : # not the first one
        target = cbox.get_active_text()
        feedback('target selected : ' + target)
    else :
        target = ''



def refreshDeviceList(widget, data) :
    cbox.clear() # delete all entries
    model = gtk.ListStore(gobject.TYPE_STRING)

    for f in os.listdir('/dev') :
        if f.find('sd') > -1 and len(f) == 3: # anything that contains sd and has three characters
            model.prepend(['/dev/%s' % f])

    model.prepend(['-- select a USB device --'])

    cbox.set_model(model)
    cell = gtk.CellRendererText()
    cbox.pack_start(cell, True)
    cbox.add_attribute(cell, 'text',0)

    cbox.set_active(0)



# radio button
def selectSource(widget, data) :
    global source
    source = data
    feedback("source selected : %s" % source)

    if data == 'iso' and widget.get_active():
        w = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                  buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        f = gtk.FileFilter()
        f.set_name("iso")
        f.add_pattern("*.iso")
        w.add_filter(f)

        res = w.run()
        if res == gtk.RESPONSE_OK:
            source = w.get_filename()
            pathLbl.set_text(source)
            feedback("source selected : " +source)
        elif res == gtk.RESPONSE_CANCEL:
            feedback('Warning, no .iso file selected!!')
            #target = 'live'
        w.destroy()




# create button
def create(widget, data):
    global thread
    
    if target == '' : # NO TARGET yet : pop warn and exit this function
        md = gtk.MessageDialog( None,
            gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING, 
            gtk.BUTTONS_CLOSE, "you must select a target device"
        )
        md.run()
        md.destroy()
        return
        
    # pop up alert to confirm target!
    md = gtk.MessageDialog( None,
            gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_QUESTION, 
            gtk.BUTTONS_YES_NO, "are you sure your target is %s ?. Warning : that this will DELETE all its content" % target
        )
    result = md.run()
    md.destroy()

    if result == gtk.RESPONSE_YES: # target confirmed
        # Are you root sir?
        stdout_txt = subprocess.Popen(["id", "-u"], stdout=subprocess.PIPE).communicate()[0]
        stdout_txt = stdout_txt[0:-1] # remove the \n character at the end

        # check if root, just in case the app was not run with gksudo
        if stdout_txt != '0' : # NOT ROOT
            md = gtk.MessageDialog( None,
                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING, 
                gtk.BUTTONS_CLOSE, "You need to be root to run this app. kthxbye."
            )
            md.run()
            md.destroy()
            sys.exit()
        else : # I AM ROOT: go ahead with live usb creation
            thread = MyThread()
            thread.start()
        
    elif result == gtk.RESPONSE_NO : # target NOT confirmed
            md = gtk.MessageDialog( None,
                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING, 
                gtk.BUTTONS_CLOSE, "Please choose a new target"
            )
            md.run()
            md.destroy()



# quit callback
def delete_event(widget, event, data=None):
    gtk.main_quit()
    return False


# about button
def about(widget, data):
    about = gtk.AboutDialog()
    about.set_program_name( abouttxt['name'] ) 
    about.set_version( abouttxt['version'] )
    about.set_copyright( abouttxt['copyright'] )
    about.set_comments( abouttxt['comments'] )
    about.set_website( abouttxt['website'] )
    about.set_logo( gtk.gdk.pixbuf_new_from_file(logopath) )
    about.run()
    about.destroy()



# help button
def dohelp(widget, data):
    md = gtk.MessageDialog( None,
                gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING, 
                gtk.BUTTONS_CLOSE, helptxt
            )
    md.run()
    md.destroy()





# requiered by thread #
gobject.threads_init()



class MyThread(threading.Thread):
    def __init__(self):
        super(MyThread, self).__init__()
        self.quit = False

    def run(self):
        while not self.quit :
            try :
                make_live_device(source, target)
            except IOError, err :
                where = "IOError"
            except shutil.Error, err:
                where = "shutil.Error"
            except OSError, err :
                where = "OSError"

            feedback('error %s %s' % (where, str(err)))
            
            self.quit = True







### GUI ###

# window
window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.set_title( "%s - %s" % (abouttxt['name'], abouttxt['version']) )
window.set_resizable(False)
window.set_border_width(5)
window.connect("delete_event", delete_event)

# GUI ##########################################
box1 = gtk.VBox(False, 0)
box1.set_border_width(10)
window.add(box1)

logo = gtk.Image()
logo.set_from_file(logopath) 
box1.pack_start(logo, True, True, 0) 
logo.show_all()


boxH = gtk.HBox(False, 0)
boxH.set_border_width(3)

sourcelabel = gtk.Label("Source : ")
boxH.pack_start(sourcelabel, True, True, 0)
sourcelabel.show()

# Live/iso radio button
button1 = gtk.RadioButton(label="live")
button1.connect("toggled", selectSource, "live") #clicked
boxH.pack_start(button1)

button2 = gtk.RadioButton(group=button1, label=".iso")
button2.connect("toggled", selectSource, "iso")
boxH.pack_start(button2)

button1.show()
button2.show()
boxH.show()

box1.pack_start(boxH)


# label to display path to .iso
pathLbl = gtk.Label('') 
box1.pack_start(pathLbl, True, True, 0)
pathLbl.show()


boxH = gtk.HBox(False, 0)
boxH.set_border_width(3)

targetlabel = gtk.Label("Target device : ")
boxH.pack_start(targetlabel, True, True, 0)
targetlabel.show()

# target Combo Box #
cbox = gtk.ComboBox()
refreshDeviceList(None, None) ### make dev list
cbox.get_focus_on_click()

cbox.connect("changed", cboxSelected, "cbox")
##cbox.connect("grab_notify", refreshDeviceList) #on open combobox

boxH.pack_start(cbox, True, True, 0)
box1.pack_start(boxH, True, True, 0)
cbox.show()

# refresh button #
ref = gtk.Button("Refresh")
ref.connect("clicked", refreshDeviceList, "refresh")
boxH.pack_start(ref, True, True, 0)
ref.show()

boxH.show()




# create button #
createB = gtk.Button("Create!")
createB.connect("clicked", create, "create button")
box1.pack_start(createB, True, True, 0)
createB.show()

boxH = gtk.HBox(False, 0)
boxH.set_border_width(3)

# about button #
aboutB = gtk.Button("About")
aboutB.connect("clicked", about, "about button")
boxH.pack_start(aboutB, True, True, 0)
aboutB.show()

# help button #
helpB = gtk.Button("Help")
helpB.connect("clicked", dohelp, "help button")
boxH.pack_start(helpB, True, True, 0)
helpB.show()

boxH.show()
box1.pack_start(boxH, True, True, 0)

progressbar = gtk.ProgressBar()
box1.pack_start(progressbar)
progressbar.show()


box1.show() # finally
window.show()



if log :
    saveout = sys.stdout
    filename = sys.argv[0].split("/")[-1]
    fsock = open('/var/log/%s.log' % filename, 'w')
    sys.stdout = fsock

gtk.main() ## MAIN LOOP ##

if log : # save log file before quiting
    sys.stdout = saveout
    fsock.close() 





