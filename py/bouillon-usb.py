#!/usr/bin/env python
# Bouillon Cube 
 
import pygtk
pygtk.require("2.0")
import gtk
 
class BouillonApp(object):       
    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file("bouillon.xml")
        builder.connect_signals({ "on_assistant_usb_cancel" : gtk.main_quit, "on_assistant_usb_prepare" : gtk.main_quit, "on_assistant_usb_apply" : gtk.main_quit })
#        builder.connect_signals({ "on_assistant_usb_prepare" : gtk.main_quit })
	self.window = builder.get_object("assistant_usb")
        self.window.show()
 
if __name__ == "__main__":
    app = BouillonApp()
    gtk.main()

