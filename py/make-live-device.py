#!/usr/bin/env python

#
# make-usb-key.py can be used to produce new USB keys from
# an existing ISO or directly from a live session
#
#    (C) Copyright 2009-2010 Puredyne Team
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import sys, os, subprocess
import time, shutil










def runCmd(cmd=''):
    print subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE).communicate()[0]
##    p = subprocess.Popen( cmd.split(' '), stdout=subprocess.PIPE).stdout
##    while 1:
##        line = p.readline()
##        if not line: break
##        print line



def make_live_device(source='live', target=''):
    # deploying the mess
    ISOMOUNT = "/mnt/make-live-iso"
    DEVMOUNT = "/mnt/make-live-device"
    
    # Are you root sir?
    #stdout_txt = os.popen("id -u", "r").read()
    stdout_txt = subprocess.Popen(["id", "-u"], stdout=subprocess.PIPE).communicate()[0]
    stdout_txt = stdout_txt[0:-1] # remove the \n character at the end
    if stdout_txt != '0' :
        print "[!] You need to be root to run this script. kthxbye."
        sys.exit()
        




    # with not enough arguments show usage and exit
    #if len(sys.argv) == 1 :
    if target == '':
        print "[!] make-live-device.sh allows you to turn any device into"
        print "[!] a Puredyne live medium, such as a liveUSB or liveHD."
        print "[!] Two arguments are required: sourceimage, and targetdevice."
        print "[!] Examples:"
        print "[!] To create a liveUSB from an ISO"
        print "[!]  Usage: " + os.getcwd() + "/" + sys.argv[0] +  " path/to/.iso /dev/yourdevice" 
        print "[!] To create a liveHD from a running liveCD" 
        print "[!]  Usage: " + os.getcwd() + "/" + sys.argv[0] +  " live /dev/yourdevice"
        sys.exit()


    # Are you sure?
    # GTK pop up here
    question = "[?] Are you sure your target is %s ? (y/n)" % target
    if raw_input(question) != 'y' :
        print "[*] kthxbye"
        sys.exit()



    # unpack the present
    if source[len(source)-4:len(source)] == ".iso" : # if four last chars are .iso
        # Mount ISO file to $ISOMOUNT
        print "[*] mounting ISO"
        if not os.path.isdir(ISOMOUNT) :
            os.mkdir(ISOMOUNT)
            
        #mount -o loop $1 $ISOMOUNT
        runCmd("mount -o loop %s %s" % (source, ISOMOUNT))

        # Calculate size of first partition
        ISOSIZEB = os.stat(source)[6] # size ?? need to remove the last character?
        ISOSIZEMB = ISOSIZEB / 1000000 #ISOSIZEMB=`expr $ISOSIZEB / 1000000`
        print "[*] ISO file is %i MB" % (ISOSIZEB)

    elif source == 'live':
        ISOMOUNT = "/live/image"
        # Calculate size of first partition
        ISOSIZEB = float(os.popen("du -sb /live/image | cut -f1", "r").read()) #`du -sb /live/image | cut -f1`
        #data = subprocess.Popen(["du", "-sb", "/live/image"], stdout=subprocess.PIPE).communicate()[0]
        #ISOSIZEMB = data.split()[0]
        ISOSIZEMB = ISOSIZEB / 1000000 #`expr $ISOSIZEB / 1000000`
        print "[*] ISO file is %i MB" % ISOSIZEMB

    else :
        print "[!] Wrong source. Please either specify an ISO file or use 'live' to use the current live system as source."
        sys.exit()

        
    # Clean target
    print "[*] force unmounting partitions from %s" % target# $2

    ##sudo umount ${2}*
    runCmd('umount %s*' % target)

    print "[*] erasing target's partition table"

    ##parted -s $2 mklabel msdos
    runCmd('parted -s %s mklabel msdos' % target)
    ##partprobe
    runCmd('partprobe')
    ##sleep 5
    time.sleep(5)
    ##umount ${2}*
    runCmd("umount %s" % target)

    # prepare system partition
    SYSPART = ISOSIZEMB + 100 #`expr $ISOSIZEMB + 100`
    print "[*] creating system partition of %i MB" % (SYSPART)

    ##parted -s $2 mkpart primary ext2 1MB ${SYSPART}MB
    runCmd("parted -s %s mkpart primary ext2 1MB %sMB" % (target, SYSPART))

    ##parted -s $2 set 1 boot on
    runCmd("parted -s %s set 1 boot on" % target)

    print "[*] force unmounting partition %s1" % (target)
    time.sleep(5)
    ##umount ${2}1
    runCmd("umount %s1" % target)

    ## mkfs.ext2 -L puredyne ${2}1
    runCmd("mkfs.ext2 -L puredyne %s1" % (target))

    # prepare persistence partition
    print "[*] creating persistence partition"
    ##parted -s $2 mkpart primary ext2 ${SYSPART}MB -- -1M
    runCmd("parted -s %s mkpart primary ext2 %sMB -- -1M" % (target, SYSPART))

    print "[*] force unmounting partition %s2" % (target)
    time.sleep(5)
    ##umount ${2}2
    runCmd("umount %s2" % target)

    ##mkfs.ext2 -L live-rw ${2}2
    runCmd("mkfs.ext2 -L live-rw %s2" % target)

    # copy live folder
    print "[*] copying live folder"
    ##if [ ! -d $DEVMOUNT ]; then
    ##    mkdir $DEVMOUNT
    ##fi
    if not os.path.isdir(DEVMOUNT) :
        print "[*] debug --> creating %s" % (DEVMOUNT)
        os.mkdir(DEVMOUNT)
            

    ##mount ${2}1 $DEVMOUNT
    runCmd("mount %s1 %s" % (target, DEVMOUNT))

    print "[*] grub configuration"

    ## this was a cramp!
    ##BLKID=`blkid | grep ${2}1 | awk '{ for(i=1;i<NF;i++) { if( $i ~ "UUID" ) { print $i } } }' | cut -d\" -f 2`
    BLKID = ''
    #stuff = os.popen("blkid", "r").read()
    stuff = subprocess.Popen(['blkid'], stdout=subprocess.PIPE).communicate()[0]
    stuff = stuff.split(' ') # convert to a list
    for i, v in enumerate(stuff) :
        if v.find( target+'1' ) > -1: # get the UUID of the 1st partition in 2nd argv 
            BLKID = stuff[i+2][6:-1] # extract id from UUID="id"

    print "[*] device ID is ", BLKID


    ##grub-install --no-floppy --root-directory=${DEVMOUNT} $2
    runCmd("grub-install --no-floppy --root-directory=%s %s" % (DEVMOUNT, target))



    ### temp fix (see launchpad bugs...)
    ##cat $(dirname "${0}")"/grub-puredyne-911.cfg.in" | sed -e 's/\${BLKID}/'${BLKID}'/g' > "${DEVMOUNT}/boot/grub/grub.cfg"
    ##cp "/usr/share/grub/unicode.pf2" "${DEVMOUNT}/boot/grub/"
    ##cp $(dirname "${0}")"/grub-puredyne-911.png" "${DEVMOUNT}/boot/grub/"


    ###FIXME !!! pass in some abs path 
    ##cat /usr/share/bouilloncube/sh/grub2/grub.cfg.in | sed -e 's/\${BLKID}/'${BLKID}'/g' >  ${DEVMOUNT}/boot/grub/grub.cfg
    txt = open('%s/grub-puredyne-911.cfg.in' % os.getcwd(), 'rU').read()
    txt = txt.replace('${BLKID}', BLKID)
    grub = open('%s/boot/grub/grub.cfg' % DEVMOUNT, 'w')
    grub.write(txt)
    grub.close()

    ##cp /usr/share/grub/unicode.pf2 ${DEVMOUNT}/boot/grub/
    shutil.copy('/usr/share/grub/unicode.pf2', DEVMOUNT+'/boot/grub/')
    shutil.copy('%s/grub-puredyne-911.png' % os.getcwd() , DEVMOUNT+'/boot/grub/')






        
    # copy all the data
    ##pushd $ISOMOUNT
    #old = os.getcwd()
    #os.chdir(ISOMOUNT)


    ## cp -rv live/ LICENSE extra md5sum.txt pure.seed README  $DEVMOUNT
    print "[*] ...copying %s/live folder to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copytree('%s/live'%ISOMOUNT, '%s/live'%DEVMOUNT)

    print "[*] ...copying %s/LICENSE file to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copy('%s/LICENSE'%ISOMOUNT, DEVMOUNT)
        
    print "[*] ...copying %s/extra folder to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copytree('%s/extra'%ISOMOUNT, '%s/extra'%DEVMOUNT)

    print "[*] ...copying %s/md5sum.txt file to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copy('%s/md5sum.txt'%ISOMOUNT, DEVMOUNT)

    print "[*] ...copying %s/pure.seed file to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copy('%s/pure.seed'%ISOMOUNT, DEVMOUNT)

    print "[*] ...copying %s/README file to %s" % (ISOMOUNT, DEVMOUNT)
    shutil.copy('%s/README'%ISOMOUNT, DEVMOUNT)

    ##pushd
    #os.chdir(old)





    # done
    print "[*] cleaning up, unmounting and ..."

    if source != 'live' : # remove file
        runCmd("umount %s" % ISOMOUNT)
        
    runCmd("umount %s" % DEVMOUNT)
    runCmd('umount %s*' % target)

    if source != 'live' : #  RECURSIVE FORCE remove file
        shutil.rmtree(ISOMOUNT) # rm -rf $ISOMOUNT

    #rm -rf $DEVMOUNT
    shutil.rmtree(DEVMOUNT) # force recursive delete file structure

    print "[*] ...done."



if __name__ == "__main__":
    make_live_device(sys.argv[1], sys.argv[2])
