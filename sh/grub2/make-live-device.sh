#!/bin/bash
#
# make-live-device.sh can be used to produce new USB keys from
# an existing ISO or directly from a live session
#
#    (C) Copyright 2009-2010 Puredyne Team
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Are you root sir?
if [ $(id -u) != "0" ]
then
	echo "[!] You need to be root to run this script. kthxbye."
	exit
fi

# deploying the mess
ISOMOUNT="/mnt/make-live-iso"
DEVMOUNT="/mnt/make-live-device"

# with not enough arguments show usage and exit
if [ -z "${1}" -o -z "${2}" ]
then
	cat <<-EOF
		[!] make-live-device.sh allows you to turn any device into
		[!] a Puredyne live medium, such as a liveUSB or liveHD.
		[!] Two arguments are required: sourceimage, and targetdevice.
		[!] Examples:
		[!]  To create a liveUSB from an ISO
		[!]    Usage: ${0} path/to/.iso /dev/yourdevice
		[!]  To create a liveHD from a running liveCD
		[!]    Usage: ${0} live /dev/yourdevice
	EOF
	exit
fi

# Are you sure?
read -p "[?] Are you sure your target is ${2} ? (y/n)"
if [ "${REPLY}" != "y" ]
then
	echo "[*] kthxbye"
	exit
fi

# unpack the present
if [ "${1##*.}" == "iso" ]
then
	# Mount ISO file to $ISOMOUNT
	echo "[*] mounting ISO"
	if [ ! -d "${ISOMOUNT}" ]
	then
		mkdir "${ISOMOUNT}"
	fi
	mount -o loop "${1}" "${ISOMOUNT}"
	# Calculate size of first partition
	ISOSIZEB=$(stat -c%s "${1}")
	ISOSIZEMB=$(expr "${ISOSIZEB}" / 1000000)
	echo "[*] ISO file is ${ISOSIZEMB} MB"
elif [ "${1}" == "live" ]
then
	ISOMOUNT="/live/image"
	# Calculate size of first partition
	ISOSIZEB=$(du -sb "/live/image" | cut -f1)
	ISOSIZEMB=$(expr "${ISOSIZEB}" / 1000000)
	echo "[*] ISO file is ${ISOSIZEMB} MB"
else
	echo "[!] Wrong source. Please either specify an ISO file or use 'live' to use the current live system as source."
	exit
fi

# Clean target
echo "[*] force unmounting partitions from ${2}"
umount "${2}"*
echo "[*] erasing target's partition table"
parted -s "${2}" mklabel msdos
partprobe
sleep 5
umount "${2}"*

# prepare system partition
SYSPART=$(expr "${ISOSIZEMB}" + 100)
echo "[*] creating system partition of ${SYSPART} MB"
parted -s "${2}" mkpart primary ext2 1MB "${SYSPART}MB"
parted -s "${2}" set 1 boot on
echo "[*] force unmounting partition ${2}1"
sleep 5
umount "${2}1"
mkfs.ext2 -L puredyne "${2}1"

# prepare persistence partition
echo "[*] creating persistence partition"
parted -s "${2}" mkpart primary ext2 "${SYSPART}MB" -- -1M
echo "[*] force unmounting partition ${2}2"
sleep 5
umount "${2}2"
mkfs.ext2 -L live-rw "${2}2"

# copy live folder
echo "[*] copying live folder"
if [ ! -d "${DEVMOUNT}" ]
then
	mkdir "${DEVMOUNT}"
fi

mount "${2}1" "${DEVMOUNT}"
echo "[*] grub configuration"

# this was a cramp!
BLKID=$(blkid | grep "${2}1" | awk '{ for(i=1;i<NF;i++) { if( $i ~ "UUID" ) { print $i } } }' | cut -d\" -f 2)
echo "[*] device ID is ${BLKID}"

grub-install --no-floppy --root-directory="${DEVMOUNT}" "${2}"

# temp fix (see launchpad bugs...)
cat $(dirname "${0}")"/grub-puredyne-911.cfg.in" | sed -e 's/\${BLKID}/'${BLKID}'/g' > "${DEVMOUNT}/boot/grub/grub.cfg"
cp "/usr/share/grub/unicode.pf2" "${DEVMOUNT}/boot/grub/"
cp $(dirname "${0}")"/grub-puredyne-911.png" "${DEVMOUNT}/boot/grub/"

# copy all the data
pushd "${ISOMOUNT}"
cp -rv live/ LICENSE extra md5sum.txt pure.seed README "${DEVMOUNT}"
pushd

# done
echo "[*] cleaning up, unmounting and ..."
if [ "${1}" != "live" ]
then
	umount "${ISOMOUNT}"
fi
umount "${DEVMOUNT}"
umount "${2}"*
if [ "${1}" != "live" ]
then
	rm -rf "${ISOMOUNT}"
fi
rm -rf "${DEVMOUNT}"
echo "[*] ...done."

